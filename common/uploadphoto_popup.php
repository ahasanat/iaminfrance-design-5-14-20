<!--upload photo modal for compose tool box-->
<div id="compose_uploadphotomodal" class="modal compose_inner_modal modalxii_level1">
   <div class="content_header">
      <button class="close_span waves-effect">
      <i class="mdi mdi-close mdi-20px"></i>
      </button>
      <p class="selected_photo_text">0 photos selected</p>
      <a href="javascript:void(0)" class="done_btn action_btn">Done</a>
   </div> 
   <div class="uploadphotomodal_container">
      <div class="row upload-images-gallery">
         <div class="col s4">
            <label  class="upload_pic_label upload_pic_camera hidden_lg">
               <span>
               <i class="zmdi zmdi-camera"></i>
               </span>
               <p class="upload_text">Camera</p>
            </label>
            <label for="upload_pic" class="upload_pic_label">
               <span>
               <i class="zmdi zmdi-upload"></i>
               </span>
               <p class="upload_text">Upload Photo</p>
               <input type="file" id="upload_pic" name="upload" class="upload_pic_input upload custom-upload" title="Choose a file to upload" required="" data-class=".main_modal.open .post-photos .img-row" multiple="">
            </label>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-2.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-3.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-4.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-5.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-6.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-2.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-3.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-4.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-5.png"></a>
         </div>
         <div class="col s4">
            <a href="javascript:void(0)" class="check-image"><div class="image-select"></div><img src="images/recent-6.png"></a>
         </div>
      </div>

   </div>
</div>