<!--preferences modal-->
<div id="preference_modal" class="modal preference_modal">
   <div class="modal_header">
      <button class="close_btn custom_modal_close_btn close_modal waves-effect">
      <i class="mdi mdi-close mdi-20px"></i>
      </button>
      <h3>Preferences</h3>
   </div>
   <div class="custom_modal_content modal_content">
      <div class="preference_label_container">
         <h5>Post in main stream</h5>
         <p class="preference_switch_label">Allow posts in the main status feed</p>
         <div class="right right_object">
            <div class="switch">
               <label>
               <input type="checkbox" id="pref_feed_switch" class="cmn-toggle cmn-toggle-round" />
               <span class="lever" for="pref_feed_switch"></span>
               </label>
            </div>
         </div>
      </div>
      <div class="preference_label_container">
         <h5>Order of posts</h5>
         <p class="preference_switch_label">Sort by recent or popular posts</p>
         <div class="right right_object">
            <div class="preference_dropdown dropdown_right">
               <a class="dropdown_text dropdown-button" href="javascript:void(0)" data-activates="pref_privacy">
               <span>
               Popular
               </span>
               <i class="zmdi zmdi-caret-down"></i>
               </a>
               <ul id="pref_privacy" class="dropdown-content custom_dropdown">
                  <li>
                     <a href="javascript:void(0)">
                     Popular
                     </a>
                  </li>
                  <li>
                     <a href="javascript:void(0)">
                     Recent
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="preference_label_container">
         <h5>Notifications</h5>
         <p class="preference_switch_label">Get notified about new posts</p>
         <div class="right right_object">
            <div class="switch">
               <label>
               <input type="checkbox" class="cmn-toggle cmn-toggle-round" id="pref_newpost_switch"/>
               <span class="lever" for="pref_newpost_switch"></span>
               </label>
            </div>
         </div>
      </div>
   </div>
</div>
