<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div>
</div>
<div class="clear"></div> 
<div class="page_container pages_container">
   <?php include("common/leftmenu.php"); ?>
   <div class="fixed-layout ipad-mfix tours-page-layout">
      <div class="content-box nbg noboxshadow">
         <div class="hcontent-holder home-section gray-section tours-page tours dine-local dine-inner-pages watch-wrapper">
            <div class="row mx-0 mainboxlt">
               <img src="images/watchphoto.png"> 
               <div class="row mx-0 boxlayout box-header">
                  <h2 class="mnlabel">Watch</h>
                  <h5 class="smllabel">Watch your favorite live shows from your home.</h5>
               </div>
            </div>
            <div class="container mt-10">
               <div class="tours-section">
                  <div class="row mx-0">
                     <ul class="collection">
                        <li class="collection-item avatar">
                           <!-- <img src="images/todo/movies/2.jpg" alt=""> -->
                           <video controls>
                              <source src="https://www.w3schools.com/html/movie.mp4" type="video/mp4">
                              <source src="https://www.w3schools.com/html/movie.mp4" type="video/ogg">
                           </video>
                           <a href="blog-detail.php">
                              <span class="title">What we are doing to support sofar artist community</span>
                              <p>
                              A dispute over escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers.
                              </p>
                              <span class="post-by">May, 02 2020</span>
                           </a>
                        </li>
                        <li class="collection-item avatar">
                           <!-- <img src="images/todo/movies/1.jpg" alt=""> -->
                           <video controls>
                              <source src="https://www.w3schools.com/html/movie.mp4" type="video/mp4">
                              <source src="https://www.w3schools.com/html/movie.mp4" type="video/ogg">
                           </video>
                           <a href="blog-detail.php">
                              <span class="title">Highlight of the week: what happening in the sofar listening room</span>
                              <p>
                              A dispute over escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers. A dispute over Huawei has escalated with implications for the firm, the tech sector and consumers.
                              </p>
                              <span class="post-by">May, 02 2020</span>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="new-post-mobile clear upload-gallery">
   <a href="javascript:void(0)"><i class="mdi mdi-plus"></i></a>
</div>

<?php include("common/footer.php"); ?>
</div>

<?php include('common/discard_popup.php'); ?>
<div id="compose_mapmodal" class="modal map_modal compose_inner_modal modalxii_level1">
   <?php include('common/map_modal.php'); ?>
</div>
<?php include("script.php"); ?>