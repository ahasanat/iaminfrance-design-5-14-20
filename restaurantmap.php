<?php include("header.php"); ?>
<?php include("common/menu.php"); ?>
<div class="floating-icon">
   <div class="scrollup-btnbox anim-side btnbox scrollup-float">
      <div class="scrollup-button float-icon"><span class="icon-holder ispan"><i class="mdi mdi-arrow-up-bold-circle"></i>
</span></div>
   </div> 
</div>
<div class="clear"></div>
<?php include("common/leftmenu.php"); ?>
<div class="fixed-layout">
   <div class="main-content hotels-page main-page places-page pb-0">
        <div class="places-mapview hasfitler showMapView">
            <div class="map-filter">
               <div class="fitem">
                  <label>Sort By</label>
                  <div class="compo">
                     <select class="select2" tabindex="-1" >
                        <option>Pricing</option>
                        <option>Ratings</option>
                     </select>
                  </div>
               </div>
               <div class="fitem">
                  <label>Hotel Class</label>
                  <div class="compo">
                     <select class="select2" tabindex="-1" >
                        <option>5 Stars</option>
                        <option>4 Stars</option>
                        <option>3 Stars</option>
                        <option>2 Stars</option>
                        <option>1 Star</option>
                     </select>
                  </div>
               </div>
               <div class="fitem">
                  <label>Guest Ratings</label>
                  <div class="compo">
                     <select class="select2" tabindex="-1" >
                        <option>5 Checks</option>
                        <option>4 Checks</option>
                        <option>3 Checks</option>
                        <option>2 Checks</option>
                        <option>1 Check</option>
                     </select>
                  </div>
               </div>
               <div class="fitem">
                  <label>Nightly Price</label>
                  <div class="compo">
                     <div class="">
                        <input type="text" class="amount" readonly>
                        <div class="slider-range"></div>
                        <div class="min-value">$500</div>
                        <div class="max-value">$10,000</div>
                     </div>
                  </div>
               </div>
               <div class="fitem">
                  <label>Amenities</label>
                  <div class="compo amenities">
                     <select class="select2" tabindex="-1"  multiple>
                        <option>Spa</option>
                        <option>Beach</option>
                        <option>Wifi</option>
                        <option>Breakfast</option>
                        <option>Pool</option>
                        <option>Transport</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="list-box moreinfo-outer">
               <div class="sidetitle">
                  <h4>Hotels in France</h4>
               </div>
               <div class="sidelist nice-scroll">
                  <div class="hotels-page">
                     <div class="hotel-list">
                        <ul>
                           <li>
                              <div class="hotel-li">
                                 <a href="javascript:void(0)" class="summery-info" onclick="openPlacesMoreInfo(this)">
                                    <div class="imgholder himg-box"><img src="images/hotel1.png" class="himg" /></div>
                                    <div class="descholder">
                                       <h4>The Guest House</h4>
                                       <div class="clear"></div>
                                       <div class="reviews-link">
                                          <span class="checks-holder">
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star"></i>
                                          <label>34 Reviews</label>
                                          </span>
                                       </div>
                                    </div>
                                 </a>
                              </div>
                           </li>
                           <li>
                              <div class="hotel-li">
                                 <a href="javascript:void(0)" class="summery-info" onclick="openPlacesMoreInfo(this)">
                                    <div class="imgholder himg-box"><img src="images/hotel2.png" class="himg" /></div>
                                    <div class="descholder">
                                       <h4>Movenpick Resort</h4>
                                       <div class="clear"></div>
                                       <div class="reviews-link">
                                          <span class="checks-holder">
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star"></i>
                                          <label>34 Reviews</label>
                                          </span>
                                       </div>
                                    </div>
                                 </a>
                              </div>
                           </li>
                           <li>
                              <div class="hotel-li">
                                 <a href="javascript:void(0)" class="summery-info" onclick="openPlacesMoreInfo(this)">
                                    <div class="imgholder himg-box"><img src="images/hotel3.png" class="himg" /></div>
                                    <div class="descholder">
                                       <h4>France Hotel</h4>
                                       <div class="clear"></div>
                                       <div class="reviews-link">
                                          <span class="checks-holder">
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star active"></i>
                                          <i class="mdi mdi-star"></i>
                                          <label>34 Reviews</label>
                                          </span>
                                       </div>
                                    </div>
                                 </a>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="moreinfo-box">
                  <a href="javascript:void(0)" onclick="closePlacesMoreInfo(this)" class="backarrow"><i class="mdi mdi-arrow-left-bold-circle"></i></a>
                  <div class="infoholder nice-scroll">
                     <div class="imgholder"><img src="images/hotel1.png" /></div>
                     <div class="descholder">
                        <h4>The Guest House</h4>
                        <div class="clear"></div>
                        <div class="reviews-link">
                           <span class="checks-holder">
                           <i class="mdi mdi-star active"></i>
                           <i class="mdi mdi-star active"></i>
                           <i class="mdi mdi-star active"></i>
                           <i class="mdi mdi-star active"></i>
                           <i class="mdi mdi-star"></i>
                           <label>34 Reviews</label>
                           </span>
                        </div>
                        <span class="distance-info">Middle Eastem &amp; African, Mediterranean</span>
                        <div class="clear"></div>
                        <div class="more-holder">
                           <ul class="infoul">
                              <li>
                                 <i class="zmdi zmdi-pin"></i>
                                 132 Brick Lane | E1 6RU, France E1 6RU, France
                              </li>
                              <li>
                                 <i class="mdi mdi-phone"></i>
                                 +44 20 7247 8210
                              </li>
                              <li>
                                 <i class="mdi mdi-earth"></i>
                                 http://www.yourwebsite.com
                              </li>
                              <li>
                                 <i class="mdi mdi-clock-outline"></i>
                                 Today, 12:00 PM - 12:00 AM
                              </li>
                              <li>
                                 <i class="mdi mdi-certificate "></i>
                                 Ranked #1 in France Hotels
                              </li>
                           </ul>
                           <div class="tagging" onclick="explandTags(this)">
                              Popular with:
                              <span>Budget</span>
                              <span>Foodies</span>
                              <span>Family</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="map-box">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3110.3465133386144!2d-9.167423685010494!3d38.77868997958898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd193295d5b45545%3A0x3f9e7b6a5f00e12c!2sPerta!5e0!3m2!1sen!2sin!4v1481089901870" width="600" height="450" frameborder="0" allowfullscreen></iframe>
               <div class="overlay">
                  <a href="javascript:void(0)" onclick="closeMapView()">Back to list view</a>
               </div>
            </div>
         </div>
   </div>
</div>
<?php include("common/footer.php"); ?>
</div>

<?php include('common/datepicker.php'); ?>

<?php include("script.php"); ?>
</body>
</html>
<script>
</script>